<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('game_events', function (Blueprint $table) {
            $table->unsignedBigInteger('game_id');
            $table->unsignedSmallInteger('team_id')->nullable();
            $table->unsignedSmallInteger('event_id');
            $table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
            $table->foreign('team_id')->references('id')->on('teams')->onDelete('cascade');
            $table->foreign('event_id')->references('id')->on('event_types')->onDelete('cascade');
            $table->boolean('is_penalty')->default(0);
            $table->unsignedTinyInteger('penalty_time')->nullable();
            $table->time('event_time');
            $table->unsignedTinyInteger("event_period");
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('game_events');
    }
};
