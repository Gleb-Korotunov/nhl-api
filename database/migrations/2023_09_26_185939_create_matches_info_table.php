<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('game_infos', function (Blueprint $table) {
            $table->unsignedBigInteger('game_id');
            $table->foreign('game_id')->references('id')->on('games')->onDelete('cascade');
            $table->tinyInteger('home_team_scored')->default(0);
            $table->tinyInteger('away_team_scored')->default(0);
            $table->boolean('is_draw')->default(0);
            $table->boolean('is_overtime')->default(0);
            $table->boolean('is_shootout')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('game_infos');
    }
};
