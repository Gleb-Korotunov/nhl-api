<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class GameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'away_team' => TeamResource::make($this->away_team),
            'home_team' => TeamResource::make($this->home_team),
            'season' => $this->season,
            'game_day' => $this->game_day,
            'game_info' => GameInfoResource::make($this->game_info)
        ];
    }
}
