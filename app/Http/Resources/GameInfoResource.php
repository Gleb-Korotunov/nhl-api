<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class GameInfoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'home_team_scored' => $this->home_team_scored,
            'away_team_scored' => $this->away_team_scored,
            'is_draw' => (bool) $this->is_draw,
            'is_overtime' => (bool) $this->is_overtime,
            'is_shootout' => (bool) $this->is_shootout
        ];
    }
}
