<?php

namespace App\Http\Controllers;


use App\Http\Resources\GameResource;
use App\Models\Game;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function getList(Request $request)
    {
        $games = Game::query()
            ->when($request->has("start_date") && $request->has("end_date"), function ($query) use ($request) {
                return $query->whereBetween('game_day', [$request->start_date, $request->end_date]);
            })
            ->get();
        return GameResource::collection($games);
    }
    public function getGame(Request $request, $id)
    {
        return GameResource::make(Game::find($id));
    }
}
