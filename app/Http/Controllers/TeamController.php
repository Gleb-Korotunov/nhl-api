<?php

namespace App\Http\Controllers;

use App\Http\Resources\TeamResource;
use App\Models\Team;
use Illuminate\Support\Facades\Request;

class TeamController extends Controller
{
    public function getList()
    {
        return TeamResource::collection(Team::all());
    }
    
    public function getTeam(Request $request, $id)
    {
        return TeamResource::make(Team::find($id));
    }
}
