<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Team extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'league_id'
    ];
    
    public function league():BelongsTo
    {
        return $this->belongsTo(League::class, 'league_id');
    }
    public function home_games():HasMany
    {
        return $this->hasMany(Game::class, 'home_team_id');
    }
    public function away_games():HasMany
    {
        return $this->hasMany(Game::class, 'away_team_id');
    }
}
