<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'event_name',
        'event_alias',
        'event_type'
    ];
    
    public function findByAlias($alias): Collection|array
    {
        return EventType::query()->where('event_alias', $alias)->get();
    }
}
