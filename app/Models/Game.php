<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Game extends Model
{
    use HasFactory;

    protected $fillable = [
        'home_team_id',
        'away_team_id',
        'source_id',
        'season',
        'game_day'
    ];
    const PRE_SEASONS_GAME_TYPE = "PR";
    const SEASONS_GAME_TYPE = "R";
    const POST_SEASONS_GAME_TYPE = "P";
    const ALL_STAR_GAME_TYPE = "WA";
    const ALL_STAR_CONTINENTAL_GAME_TYPE = "A";
    
    public function home_team(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'home_team_id');
    }
    public function away_team(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'away_team_id');
    }
    public function game_events(): HasMany
    {
        return $this->hasMany(GameEvent::class, 'game_id');
    }
    public function game_info(): HasOne
    {
        return $this->hasOne(GameInfo::class, 'game_id');
    }
}
