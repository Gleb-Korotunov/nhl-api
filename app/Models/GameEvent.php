<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class GameEvent extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'game_id',
        'team_id',
        'event_id',
        'event_time',
        'is_penalty',
        'penalty_time',
        'event_period'
    ];
    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
    public function event(): BelongsTo
    {
        return $this->belongsTo(EventType::class, 'event_id');
    }
}
