<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GameInfo extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
      'game_id',
      'home_team_scored',
      'away_team_scored',
      'is_draw',
      'is_overtime',
      'is_shootout'
    ];
}
