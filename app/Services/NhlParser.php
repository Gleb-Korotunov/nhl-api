<?php
namespace App\Services;

use App\Models\EventType;
use App\Models\Game;
use App\Models\GameEvent;
use App\Models\GameInfo;
use App\Models\Team;
use DateTime;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use JetBrains\PhpStorm\NoReturn;

class NhlParser {

    protected string $startDate;
    protected string $endDate;
    protected Client $client;
    public function __construct($startDate, $endDate)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->client = new Client();
    }
    #[NoReturn] private function  dataDistribution($gameData, $EventJSON, $status = false):void
    {
        $homeTeam = Team::firstOrCreate([
            'name' => $gameData['home_team_id'],
            'league_id' => 1
        ]);
        $awayTeam = Team::firstOrCreate([
            'name' => $gameData['away_team_id'],
            'league_id' => 1
        ]);
        $gameData['home_team_id'] = $homeTeam->id;
        $gameData['away_team_id'] = $awayTeam->id;
        $dateTime = new DateTime($gameData['game_day']);
        $gameData['game_day'] = $dateTime->format('Y-m-d');
        $gameModel = Game::firstOrCreate($gameData);
        if ($status != "Final") return;
        $gameInfoData = ['game_id' =>  $gameModel->id];
        foreach ($EventJSON->liveData->linescore->periods as $periodItem) {
            if ($periodItem->periodType === "OVERTIME") {
                $gameInfoData['is_overtime'] = 1;
                $gameInfoData['is_draw'] = 1;
            }
        }
        $gameInfoData['is_shootout'] = (int) $EventJSON->liveData->linescore->hasShootout;
        $gameInfoData['home_team_scored'] = $EventJSON->liveData->linescore->teams->home->goals;
        $gameInfoData['away_team_scored'] = $EventJSON->liveData->linescore->teams->away->goals;
        GameInfo::firstOrCreate($gameInfoData);
        foreach ($EventJSON->liveData->plays->allPlays as $item) {
            if ($item->about->eventIdx < 3) continue;
            $eventPayload = [];
            $eventPayload['event_alias'] = strtolower($item->result->eventTypeId);
            $eventPayload['event_name'] = $item->result->event;
            $eventPayload['event_type'] = $item->result->penaltySeverity ?? null;
            $eventTypeModel = EventType::firstOrCreate($eventPayload);
            $eventPenaltyTime = $item->result->penaltyMinutes ?? null;
            $gameEventPayload = [];
            $gameEventPayload['game_id'] = $gameModel->id;
            $gameEventPayload['event_id'] = $eventTypeModel->id;
            $gameEventPayload['event_time'] = $item->about->periodTime;
            $gameEventPayload['event_period'] = $item->about->period;
            $gameEventPayload['is_penalty'] = $eventPenaltyTime ?? 0;
            $gameEventPayload['penalty_time'] = $eventPenaltyTime;
            if (isset($item->team) && $homeTeam->name == $item->team->name) {
                $gameEventPayload['team_id'] = $homeTeam->id;
            }
            if (isset($item->team) && $awayTeam->name == $item->team->name) {
                $gameEventPayload['team_id'] = $awayTeam->id;
            }
            GameEvent::firstOrCreate($gameEventPayload);
        }
    }
    public function parseGameList():void
    {
        $gamesListJSON = $this->getGamesListJson();
        $dateList = $gamesListJSON->dates;

        foreach ($dateList as $dateItem) {
            echo "FOUND GAMES BY DATE {$dateItem->date} , total: {$dateItem->totalGames}".PHP_EOL;
            if ($dateItem->totalGames === 0) continue;

            foreach ($dateItem->games as $gameItem) {
                if ($gameItem->gameType !== Game::SEASONS_GAME_TYPE) continue;
                if (Game::query()->where('source_id', $gameItem->gamePk)->first()) continue;
                $payload = [
                    'source_id' => $gameItem->gamePk,
                    'season' => substr($gameItem->season,0,4),
                    'game_day' => $gameItem->gameDate,
                    'home_team_id' => $gameItem->teams->home->team->name,
                    'away_team_id' => $gameItem->teams->away->team->name
                ];
                $gameEvenetList = $this->getGameJson($gameItem->gamePk);
                $this->dataDistribution($payload, $gameEvenetList, $gameItem->status->abstractGameState);
            }
        }
    }

    private function getGameJson($id) {
        try {
            return json_decode(
                $this->client->get($this->getGameEventsUrl($id))
                ->getBody()
                ->getContents()
            );
        }
        catch (ClientException|ServerException|GuzzleException $exception) {
            var_dump($exception->getCode(), $exception->getMessage());
            return false;
        }
    }
    private function getGamesListJson() {
        try {
            return json_decode(
                $this->client->get($this->getGameListUrl($this->startDate, $this->endDate))
                ->getBody()
                ->getContents()
            );
        }
        catch (ClientException|ServerException|GuzzleException $exception) {
            var_dump($exception->getCode(), $exception->getMessage());
            return false;
        }
    }
    private function getGameEventsUrl(int $id): string
    {
        return sprintf("https://statsapi.web.nhl.com/api/v1/game/%d/feed/live", $id);
    }
    private function getGameListUrl($startDate, $endDate): string
    {
        return sprintf("https://statsapi.web.nhl.com/api/v1/schedule?expand=schedule.brodcasts&startDate=%s&endDate=%s",$startDate, $endDate);
    }
}
