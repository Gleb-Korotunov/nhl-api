<?php

namespace App\Console\Commands;


use App\Models\Country;
use App\Models\League;
use App\Services\NhlParser;
use Illuminate\Console\Command;

class ParseNhlArchive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:parse-nhl-archive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
//        $country = Country::firstOrCreate([
//        'name' => 'United States'
//    ]);
//League::firstOrCreate([
//        'name' => 'NHL',
//        'country_id' => $country->id
//    ]);
        $service = new NhlParser("2018-09-10","2023-12-10");
        $service->parseGameList();
    }
}
