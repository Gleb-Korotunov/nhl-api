<?php

use App\Http\Controllers\GameController;
use App\Http\Controllers\TeamController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::prefix('v1')->group(function () {
    Route::get('command/list', [TeamController::class, "getList"])->name("app.command.list.get");
    Route::get('command/{id}', [TeamController::class, "getTeam"])->name("app.command.get");
    Route::get('game/list', [GameController::class, "getList"])->name("app.command.game.list.get");
    Route::get('game/{id}', [GameController::class, "getGame"])->name("app.command.game.get");
});
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
